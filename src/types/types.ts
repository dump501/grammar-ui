export type Program = {
  kind: string | undefined;
  type: string | undefined;
  instructions: Array<Instruction> | undefined;
};

export type Instruction = {
  kind: string | undefined;
  directive: Directive | undefined;
  val: Nombre | undefined;
};

export type Directive = {
  kind: string | undefined;
  type: string | undefined;
  value: string | undefined;
};

export type Nombre = {
  kind: string | undefined;
  type: string | undefined;
  value: number;
};
