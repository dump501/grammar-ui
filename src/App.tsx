import { useState } from "react";
import "./App.css";
import parser from "./game";
import { Instruction, Program } from "./types/types";

function App() {
  const [input, setinput] = useState("");
  const [ast, setast] = useState<Program>();
  const buildAst = () => {
    try {
      let t = parser.parse(input);
      console.log(t);
      setast(t);
    } catch (error) {
      alert("le mot n'est pas reconnu par la grammaire");
      setast(undefined);
    }
  };
  return (
    <div className="wrapper">
      <div className="input">
        <div style={{ display: "flex", flexDirection: "column" }}>
          <textarea
            onChange={(e) => {
              setinput(e.target.value);
            }}
            className="input_text"
            placeholder="Entrer les caratères ici"
          ></textarea>
          <br />
          <button
            onClick={() => {
              buildAst();
            }}
          >
            Tester le programme
          </button>
        </div>
        <div>
          <h3>Resultat</h3>
          {ast ? (
            <h4>le mot est Accepté</h4>
          ) : (
            <h4>le mot n'est pas Accepté</h4>
          )}
        </div>
      </div>
      <div>
        <h3>Arbre de derivation</h3>
        {ast && (
          <div
            className="tree"
            style={{ overflowX: "scroll", minWidth: "300vw" }}
          >
            <ul>
              <li>
                <div>Program</div>
                <ul>
                  {ast.instructions!.map((instruction: Instruction) => {
                    return (
                      <li>
                        <div>{instruction.kind}</div>
                        <ul>
                          <li>
                            <div>{instruction.directive!.kind}</div>
                            <ul>
                              <li>
                                <div>{instruction.directive!.value}</div>
                              </li>
                            </ul>
                          </li>
                          <li>
                            <div>{instruction.val!.kind}</div>
                            <ul>
                              <li>
                                <div>{instruction.val!.value}</div>
                              </li>
                            </ul>
                          </li>
                        </ul>
                      </li>
                    );
                  })}
                </ul>
              </li>
            </ul>
          </div>
        )}
      </div>
    </div>
  );
}

export default App;
